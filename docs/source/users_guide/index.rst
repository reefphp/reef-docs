
User's guide
================================

Reef provides a user interface for both managing and filling out forms. This user's guide is intended to as documentation on how to manage forms using the form builder UI. For technical documentation on the Reef API or on extending Reef, please refer to the integration guide or contribution guide, respectively.

.. _terminology:

Terminology
---------------
Before we start, let's agree on some terminology first:

* A *component* is a type of input, e.g. a text input, dropdown or checkbox
* A *field* is a component inserted in a form. You may for example add a text input component to ask for a user's name, and use another text input component to ask for his favourite type of food
* A *form* is a collection of fields, e.g. you may compose a form asking for a user's name, age and gender using a text field, number field and radio (bullet selection) field
* A *submission* is form data submitted by the user

Additionally, the following terminology is used in configurations:

* A *title* represents text that is presented to the end user, often the user viewing a form. For installations using multiple locales, titles can always be translated in these multiple locales.
* A *name* on the other hand represents the technical name of some object, for instance a field. A name may only consist of lowercase alphanumeric characters and underscores, that is ``a-z``, ``0-9`` and ``_``, under the condition that the name starts with a letter (``a-z``) and an underscore may not be followed by another underscore. These technical names are used internally to identify the object you are configuring: the storage in the database will be named after it, and the name can be used in conditions. In the builder, all objects always get assigned a valid default name, so you are not required to name objects yourself, but you can change the name if necessary.

Outline
---------------
The process of creating a new form typically consists of:

#. Adding fields to your form
#. Configuring each field
#. Optionally, configuring the form itself
#. Saving the form

These actions have their own tabs in the interface, available in the top left.

#. In the leftmost tab, all available components are displayed, which you can drag into your form, or on which you can click to append them to your form
#. When selecting a field you have added, the middle tab provides the possibility to configure the field in question
#. The rightmost tab may (or may not, depending on the integration) provide the possibility for some form configuration, after which you can also save the form here

Configuring fields
------------------
The fields are divided into a number of groups:

* Text input fields, in which users can type data
* Choice fields, where users can choose from a predefined set of choices
* Static fields, e.g. headings and paragraphs of text, that do not present any input possibility
* 'Other' fields, including any fields that do not fit in any of the above categories

Each component has its own specific set of configuration possibilities. A heading is quite simple: you can set the size and the text. A number field can be configured quite extensively, providing possibilities to set e.g. a minimum, maximum and default value, and the possibility to set custom error feedback texts.

The specific configuration settings of all components can be found in the `components reference <../components/index.html>`__. Here, we will focus on two general settings applying to almost all components.

Visible setting
~~~~~~~~~~~~~~~
You may wish to optionally show or hide fields, depending on data entered previously. For example, you may want to ask for one's telephone number after they have checked a checkbox asking whether they want to get in touch. In this case, the telephone number field would be shown if the checkbox is ticked, or hidden otherwise. You can achieve this by setting the 'visible' setting to 'condition', and configuring this property in the correct way. More on how to do this can be found on the `condition component page <../components/reef/condition.html>`__.

Required setting
~~~~~~~~~~~~~~~~
Analogous to the visible setting, you may want to optionally require some fields. For instance, in the above example, instead of hiding the telephone field if the box is not ticked, you may wish to show the field but not require it, and only require it if the user indicates he wants to get in touch. In this case you can use a condition for the required property. Of course you can also set it to 'Yes' or 'No' to either always or never require it. Again, more information on conditions can be found on the `condition component page <../components/reef/condition.html>`__.
