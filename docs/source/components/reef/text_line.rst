
Text line
================================

The text line component allows the user to fill in a freeform text value consisting of one single line.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Default value
	    | ``default``
	  - Text
	  - '' (empty)
	  - The default value
	* - | Max. input length
	    | ``max_length``
	  - Number
	  - 1000
	  - The maximum input length, in number of characters

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
	* - | Placeholder
	    | ``placeholder``
	  - The default gray background text
