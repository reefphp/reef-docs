
Paragraph
================================

The paragraph component allows a form creator to add a paragraph of plain text.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* -
	  - 
	  - 
	  - This component has no declaration parameters

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Content
	    | ``content``
	  - The paragraph text
