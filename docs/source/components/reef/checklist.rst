
Checklist
================================

The checklist component presents the user with a list of checkboxes, each of which can be independently checked.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Options
	    | ``options``
	  - Option list
	  - 
	  - Each option added here represents a single checkbox that can be checked. The checkbox indicates the default value of that option, either checked or not checked. At least two options should be added. See `Option list <./option_list.html>`__ for more details.

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
