
Choice list (radio buttons)
================================

The choice list component allows the user to choose one of multiple options. In contrast to the `Dropdown (select) <./select.html>`__ component, all options are always presented, with bullet points before the options that can be clicked to choose that option.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Options
	    | ``options``
	  - Option list
	  - 
	  - Each option added here represents a radio bullet to choose from. The checkbox indicates the default value (at most one). At least two options should be added. See `Option list <./option_list.html>`__ for more details.

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
