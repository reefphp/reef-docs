
Condition
================================

The condition component is an internal component, and hence cannot be used in the builder. It is used as configuration component in the builder however.

Conditions in the builder
--------------------------------

Conditions provide you the possibility to perform certain actions based on values that the user filled in previously. If you have two fields and only want to show field 2 if field 1 is filled in, this can be achieved using conditions. Reef itself provides functionality to show or hide fields dependent on certain conditions, and to mark fields as required based on certain conditions. These two settings are called 'visible' and 'required' respectively, for these settings, conditions should be read as follows:

 * visible: *This field should be visible only if the following is true*
 * required: *This field should be required only if the following is true*

.. note::
   Invisible fields cannot be required. However, you do not need to check this yourself: Reef checks this for you

In the builder, a condition can be set by either of four methods:

 * 'Yes' (or ``true``), indicating a positive result (e.g. the field should be visible)
 * 'No' (or ``false``), indicating a negative result (e.g. the field should not be visible)
 * 'Condition', this option provides you with an interface to build a custom condition. More information below.
 * 'Manual condition', this option is only provided for those who have enough technical knowledge to type conditions themselves, and wish to create more complicated conditions

The condition interface
----------------------------------

Any condition returns either yes/true or no/false (the first two of the four options above). Which of these two, depends on the input of the user. In the interface, you can select three fields. Always select them from left to right!

 * The first dropdown allows you to select the field which has to be used in the condition. By default, Reef assigns names like ``field_123456789abcdef`` to fields, but you may change these names in order to ease the condition building
 * Dependent on the choice in the first dropdown, the second dropdown present different operators you can use. These could for instance be checks for equality, checking a number is higher/lower than some number, et cetera
 * Often, an operator in the second dropdown requires some operand, which can be chosen in the third and final input. The exact behaviour of this last input varies per field and operator. Refer to their documentation for more information.

The above holds for a single condition. However, you may wish to combine multiple conditions. E.g., ask *A* if *B* or *C*. You can add multiple conditions using an and/or construction: ``B and C`` is true if both ``B`` and ``C`` are true, ``B or C`` is true if at least one of ``B`` and ``C`` is true.

Technical details
--------------------------------

The option list allows the user to provide multiple options in multiple locales. Besides a name and localized title, a checkbox can be utilized to provide a toggle for default options.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Default
	    | ``default``
	  - Condition
	  - None
	  - A default condition

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
