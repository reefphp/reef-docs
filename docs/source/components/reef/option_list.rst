
Option list
================================

The option list component is an internal component, and hence cannot be used in the builder. It is used as configuration component in the builder however.

Option list in the builder
--------------------------------

For components involving multiple predefined options, an option list is presented allowing you to fill in the options the user is allowed to choose from. For each option, there are three settings to configure:

 * Name: The name of the options. This is a technical name that can be used for e.g. conditions and overviews, and hence should only consist of lowercase letters, numbers and underscores. If the names are not important, the default names ``option_1``, ``option_2``, et cetera are perfectly fine to use. For more information on names, please refer to the `terminology <../../users_guide/index.html#terminology>`__.
 * Title: The title of the option. This is the text that is presented to the user. When using multiple locales, you can enter a different text for each locale.
 * Checkbox: The checkbox often indicates whether the option is a default option, i.e. whether it is choosen by default or not.

Both the name and checkbox may be absent from the option list, depending on the particular needs of the component.


Technical details
--------------------------------

The option list allows the user to provide multiple options in multiple locales. Besides a name and localized title, a checkbox can be utilized to provide a toggle for default options.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Min. number of options
	    | ``min_num_options``
	  - Number
	  - 2
	  - The minimum amount of options the user should provide. Often, this should be 2
	* - | Max. number of options
	    | ``max_num_options``
	  - Number
	  - None
	  - The maximum amount of options the user may provide. By default, no explicit limit is used
	* - | Min. number of checked defaults
	    | ``max_checked_defaults``
	  - Number
	  - None
	  - The maximum amount of options the user may mark as default
	* - | Default number of options
	    | ``default_num_options``
	  - Number
	  - 3
	  - The default number of options that is added when in the builder a new field is added that uses an option list in its configuration
	* - | Ask field names
	    | ``names``
	  - Checkbox
	  - true
	  - Whether names should be asked. Toggles the display of the name input fields
	* - | Default options
	    | ``default``
	  - Option list
	  - ``default_num_options`` default options
	  - A list of default options

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
