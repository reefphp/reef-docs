
Heading
================================

The heading component allows a form creator to add a title or heading to a form.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Level
	    | ``level``
	  - Number
	  - 1
	  - The heading level, may be a number from 1 to 6. A level 1 heading corresponds to the HTML ``<h1>`` tag. Generally the level 1 heading is the largest heading, while the a level 6 heading is the smallest.

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The title text
