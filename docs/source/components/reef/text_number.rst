
Number
================================

The number component allows the user to fill in a number. It adheres to the min/max/step construction that the HTML element ``<input type="number">`` uses.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Default value
	    | ``default``
	  - Number
	  - '' (empty)
	  - The default value
	* - | Minimum
	    | ``min``
	  - Number
	  - '' for none
	  - The minimum value
	* - | Maximum
	    | ``max``
	  - Number
	  - '' for none
	  - The maximum value
	* - | Step
	    | ``step``
	  - Number
	  - '', implies '1'
	  - The step between values. Valid field values are defined by the sequence ``min``, ``min + step``, ``min + 2*step``, et cetera not exceeding ``max``.

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
	* - | Validation error
	    | ``error_not_a_number``
	  - Feedback text when the user has entered non-numeric input
	* - | Minimum error
	    | ``error_number_min``
	  - Feedback text when the user has entered a value that is too low
	* - | Maximum error
	    | ``error_number_max``
	  - Feedback text when the user has entered a value that is too high
	* - | Min/max error
	    | ``error_number_min_max``
	  - Feedback text when the user has entered a value that is not within the set bounds
