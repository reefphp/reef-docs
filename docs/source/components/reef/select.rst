
Dropdown (select list)
================================

The dropdown component allows the user to choose one of multiple options. In contrast to the `Choice list (radio buttons) <./radio.html>`__ component, only one options is presented, with the possibility to extend the field to show all options to choose from.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Options
	    | ``options``
	  - Option list
	  - 
	  - Each option added here represents an option to choose from. The checkbox indicates the default value (at most one). At least two options should be added. See `Option list <./option_list.html>`__ for more details.

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
