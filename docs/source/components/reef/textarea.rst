
Textarea
================================

The textarea component allows the user to fill in a (possibly large) freeform text value consisting of possibly multiple lines.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Default value
	    | ``default``
	  - Text
	  - '' (empty)
	  - The default value
	* - | Max. input length
	    | ``max_length``
	  - Number
	  - 15000
	  - The maximum input length, in number of characters

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
	* - | Placeholder
	    | ``placeholder``
	  - The default gray background text
