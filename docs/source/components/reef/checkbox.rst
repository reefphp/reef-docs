
Checkbox
================================

The checkbox component presents the user with a single checkbox.

.. list-table:: Declaration parameters
	:widths: 10 10 10 70
	:header-rows: 1
	
	* - Name
	  - Type
	  - Default value
	  - Description
	* - | Checked by default
	    | ``default``
	  - Checkbox
	  - 
	  - This option indicates the default value of the checkbox, either checked or not checked

.. list-table:: Language items
	:widths: 20 80
	:header-rows: 1
	
	* - Name
	  - Description
	* - | Title
	    | ``title``
	  - The field title/label
	* - | Box label
	    | ``box_label``
	  - A label near the checkbox
