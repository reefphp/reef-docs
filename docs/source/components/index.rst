
Components reference
================================

Click on a reference below to view its guide.

Text
----

Text components allow the user to enter text. The entered value may be anything within the set boundaries.

.. toctree::
   :maxdepth: 1

   reef/text_line
   reef/textarea
   reef/text_number

Choice
------

Choice components allow the user to choose between a predefined set of values, i.e. no manual input is requested.

.. toctree::
   :maxdepth: 1

   reef/checkbox
   reef/checklist
   reef/radio
   reef/select

Static
------

Static components allow the creator of a form to add some text without an input field, to add some contextual information to the form.

.. toctree::
   :maxdepth: 1

   reef/heading
   reef/paragraph


Internal
--------

Internal components are only meant to be used in the builder UI, hence they can not be used in forms. However, due to their more elaborate configuration options, the following pages are still useful to end users.

.. toctree::
   :maxdepth: 1

   reef/option_list
   reef/condition
