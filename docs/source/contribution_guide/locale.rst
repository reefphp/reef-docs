
Adding a locale
================================

In contrast to other plugin types, for locales no PHP classes are required to be implemented.
You only have to define the translations of the locale in question.

You can either write the required locale files and send a merge request to Reef including them, or add locales on the fly using ``setLocale()``.
The locale name is an *IETF language tag* with the dash (-) replaced with an underscore (_). All plugins should always include a ``en_US`` locale, others may be added as desired.

Adding a locale to Reef (merge request)
----------------------------------------

If you want to add locale files to Reef in a merge request, you will have to translate the following files into your desired locale:

 - ``locale/en_US.yml``
 - ``src/Components/{{component_name}}/locale/en_US.yml`` (for each component)

Create a merge request containing translations of these files, and we may merge them into Reef.

Adding/setting locale on the fly
--------------------------------

You may want to override some locale or add some custom locale. This can be done on multiple levels:

1. At the very base, Reef defines some language strings in ``locale/en_US.yml``, which can be edited using ``\\Reef\\Reef::setLocale()``.
2. On top of the Reef locale, language strings may also be set on component level by using ``\\Reef\\Components\\Component::setLocale()``. These can also be set in the component locale file residing in ``locale/en_US.yml`` relative to the component directory.
3. On top of the Reef locale, language strings may also be set on form level by using ``\\Reef\\Form\\Form::setLocale()``.
4. On top of the Form and Component locale, language strings may also be set on field level by using ``\\Reef\\Components\\Field::setLocale()``. For a field, translations are fetched in the following order or prevalence: field, form, component, reef.

The ``setLocale(string $s_locale, string[] $a_locale)`` method is shared between these classes, and receives the locale name as its first parameter, and list of translations as second parameter.
