
Contribution guide
================================

There are multiple ways you can customize, extend or contribute to Reef. Most of these can be done by using mechanisms provided by Reef, such as for layouts or components. A guide for each of these types is available below:

.. toctree::
   :maxdepth: 1

   component/index
   locale
   layout
   extension
   iconset
