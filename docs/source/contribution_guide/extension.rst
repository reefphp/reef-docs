
Creating an extension
================================

Extensions can be used to extend Reef in ways that are not possible using any of the other methods. You can add extensions to for example add configuration values to components or add some HTML to fields.

Extension member functions
--------------------------------

Extensions should extend the abstract class ``\Reef\Extension\Extension``.
Each extension should have a unique name returned by the ``getName()`` method, and the directory that the extension code resides in should be given by the ``getDir()`` method.
Just as with components, the ``getDir()`` implementation should be something along the lines of::

	public static function getDir() : string {
		return __DIR__.'/';
	}

In addition, extensions have the possibility to use a number of different methods:

``__construct()``
````````````````````````````````

The ``__construct()`` function is not used by the ``Extension`` base class, and hence can without limitations be used by the extension itself for initialization purposes.

``init()``
````````````````````````````````

The ``init()`` function can be used to initialize the extension. It is called as soon as the extension is added to the Reef setup class.
The difference with the ``__construct()`` method is that ``init()`` receives the Reef setup object as first parameter, allowing you to use the entire Reef setup here.

``getCSS()`` and ``getJS()``
````````````````````````````````
Just as with components, you can use the ``getCSS()`` and ``getJS()`` functions to add any (remote) CSS/JS assets the extension requires.

``checkSetup()``
````````````````````````````````

The ``checkSetup()`` function can be used to check the extension configuration. It is called by the ``checkSetup()`` function in the Reef setup class, which is called as soon as a ``Reef`` object is created.
In this method you can check whether any dependencies are present that should be present, and whether there are any incompatibilities.

``nativelySupportedLayouts()``
````````````````````````````````

The ``nativelySupportedLayouts()`` function can be used to override the layouts supported by the extension, if it uses templating. If you do not, you may override this function to return ``null``.

``addLayout()``
````````````````````````````````

Probably you cannot support each layout that's around.
Hence, if a layout wishes to build in support for this extension, the ``addLayout()`` function can be used by the layout to add a layout directory for that layout for this extension.

``getSubscribedEvents()``
````````````````````````````````

This function can be used to define listeners to events. Events are triggered by Reef (or e.g. an other extension) in order for extensions to be able to hook on.
Each event has a unique name, in the form ``{vendor}.{event_name}``. The ``getSubscribedEvents`` function returns an associative array defining a mapping between event names and function names.
For example, if you wish to perform the (self-defined) extension member function ``component_configuration()`` when the ``reef.component_configuration`` event is triggered, you may use::

	public static function getSubscribedEvents() {
		return [
			'reef.component_configuration' => 'component_configuration',
		];
	}

The ``component_configuration()`` function then receives an associative array of variables passed by the event. Make sure you fetch these by reference in order to be able to edit them::

	public function component_configuration(&$a_vars) {
		// ...
	}

Layout hooks
--------------------------------

You can add HTML to layouts using layout hooks. A template hook is defined by a template hook tag ``[[...]]`` containing a template hook name, e.g. ``[[form_input_after]]``, a hook after the input in a component form template.
If you define a file ``./view/{layout_name}/{hook_name}.mustache`` in your extension, this template will be inserted at the position of the hook in the HTML.
The hook tags are preprocessed and cached by Reef before being passed to Mustache, hence:

1. you should empty the Reef cache after editing a hook file, as Reef does not check modifications of hook files for performance reasons,
2. you have complete access to the entire scope of the mustache template you are hooking into, including any variables you may have added using the ``reef.component_configuration`` event.
