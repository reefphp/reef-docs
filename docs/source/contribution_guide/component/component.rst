
Creating the component & component class
=========================================

Components define specific types of input: a text field, checkbox, dropdown, et cetera. Reef provides some basic components, but you can write components yourself to introduce custom input types.

Component member functions
--------------------------------

The main component class should extend the abstract class ``\Reef\Components\Component``.
Each component should have a unique name set by the ``const COMPONENT_NAME`` constant, and the directory that the component code resides in should be given by the ``getDir()`` method.
The ``getDir()`` implementation should be something along the lines of::

	public static function getDir() : string {
		return __DIR__.'/';
	}

In addition, components have the possibility to use a number of different methods:

``__construct()``
````````````````````````````````

The ``__construct()`` function is not used by the ``Component`` base class, and hence can without limitations be used by the component itself for initialization purposes.

``init()``
````````````````````````````````

The ``init()`` function can be used to initialize the component. It is called as soon as the component is added to the Reef setup class.
The difference with the ``__construct()`` method is that ``init()`` receives the Reef setup object as first parameter, allowing you to use the entire Reef setup here.

``getCSS()`` and ``getJS()``
````````````````````````````````
You can use the ``getCSS()`` and ``getJS()`` functions to add any (remote) CSS/JS assets the component requires.
Each of these methods should return an array of arrays, where each contained array is an associative array defining an asset using the following entries::

	type => 'local' (in component directory) or 'remote' (over http(s))
	path => path or url to the file
	view => for which view(s) to load, one of 'form', 'submission', 'builder' or 'all'. Optional, defaults to 'all'
	name => canonical name (required for remote files). Allows Reef to recognize when two plugins want to include the same library, and to make sure it does not include them twice.
	integrity => Optionally, an integrity value for remote files

``checkSetup()``
````````````````````````````````

The ``checkSetup()`` function can be used to check the component configuration. It is called by the ``checkSetup()`` function in the Reef setup class, which is called as soon as a ``Reef`` object is created.
In this method you can check whether any dependencies are present that should be present, and whether there are any incompatibilities.

``validateDeclaration()``
````````````````````````````````

The ``validateDeclaration()`` function is performed whenever a field declaration (possibly belonging to a form definition) is validated. You can override it to perform any custom checks. For example, the ``reef:text_number`` components checks whether the range ``max`` is not lower than the range ``min``. By default this function returns ``true`` indicating a valid declaration, but this may change in the future, so be sure to call this method and use its result in your child class implementation.
If you require the form context of the field in your check, you may better use ``Field::validateDeclaration()``.

``requiredComponents()``
````````````````````````````````

The ``requiredComponents()`` function returns an array of component names that are required to be present for the component to work. The main component class determines this list automatically based on the ``basicDefinition`` and ``advancedDefinition`` of the component configuration. However, you may need to augment this array a bit in specific cases.

``nativelySupportedLayouts()``
````````````````````````````````

The ``nativelySupportedLayouts()`` function can be used to override the layouts supported by the component, if it uses templating. If you do not, you may override this function to return ``null``.

``addLayout()``
````````````````````````````````

Probably you cannot support each layout that's around.
Hence, if a layout wishes to build in support for this component, the ``addLayout()`` function can be used by the layout to add a layout directory for that layout for this component.

``getConfiguration()``
````````````````````````````````

This function returns the configuration array of the component. The result is cached for performance. The configuration is determined based on the component and its parent components; however do note that component inheritance is not fully developed yet.

``internalRequest()``
````````````````````````````````

Internal requests routed to this component arrive here. Internal requests serve as communication method between the browser and PHP, to aid interactivity. Depending on the request hash (the first argument), you may return anything to the browser, like an image, JSON object, et cetera.
The request paths of components are formed like ``component:{component_vendor_name}:{component_component_name}:{custom_request_hash}``, where ``custom_request_hash`` is the hash passed to ``Component::internalRequest()``.
