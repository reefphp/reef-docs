
Configuration file
================================

The configuration file defines most of the settings of your component. If contains the name, used locale entries, declaration form and condition operators.

In general, your ``config.yml`` will look like this::

	vendor: vendor_name
	name: component_name
	category: text
	documentation: https://link-to-your-docs
	assets:
		component_image: img/component-image.svg
	basicLocale:
		title: rf_field_title
	advancedLocale:
		error_too_long: error_too_long_title
	internalLocale:
		error_empty:
	basicDefinition:
		fields: []
	advancedDefinition:
		fields: []
	props: []
	builder_operators:
		empty: is empty
		nempty: is not empty

Required values
--------------------------------

The vendor name and component name should be names according to the conventional definition within Reef. The category can be one of the builder categories, e.g. text, choice, static or other. These three values (vendor, name, category) are always required.
The ``component_image`` defines the path to the image file of this component, used in the builder. When the component is to be used in the builder, this value is also required.

Optional values
--------------------------------

Locale and definition
````````````````````````````````

The basic/advanced variants define whether to place the specific item in the basic tab or advanced tab in the builder.
The ``basicLocale`` and ``advancedLocale`` contain ``key: value`` pairs where:

 - ``value`` is the locale name of the title of the text input in the builder, i.e. the text presented to the form maker. These are typically also present in a locale .yml file, either the component locale or the general reef locale.
 - ``key`` is the locale name of the locale value, i.e. the text filled in by the form maker and presented to the form submitter

Additionally, ``internalLocale`` is an array of locale keys in the form ``key: (empty)``, where each key is a value present in the the component or reef locale files.

The definition arrays contain a (possibly empty) list of field declarations. These declarations define the custom configuration values for the component, e.g. a default value, placeholder, max input length, et cetera.
The ``name``, ``required`` and ``visible`` fields are automatically added by Reef, depending on the PHP implementations.

You may also have a ``props`` entry, which may hold a value that should not be presented in the builder. It is for example used in the ``text_line`` component to define a regexp value. Each entry in props is a hash with both a name and a default value.


Operators and props
````````````````````````````````
There is also the ``builder_operators`` entry, defining which condition operators are supported. It holds an array of ``key: value`` pairs where:

 - ``key`` is the name of the operator, which is used to translate the operator name in the locale file (where ``operator_{operator name}`` is used)
 - ``value`` is the operator as used in conditions. This should be an english name, just as in most programming languages
