
The field class
================================

Field are instances of components, in the sense that a field is a component assigned to a form. The form and component objects can be fetched using ``getForm()`` and ``getComponent()``.

Field member functions
--------------------------------

The field class should extend the abstract class ``\Reef\Components\Field``.

Fields have the possibility to use a number of different methods:

``__construct()``
````````````````````````````````

The ``__construct()`` function is not used by the ``Field`` base class, and hence can without limitations be used by the component itself for initialization purposes.

``getDeclaration``
````````````````````````````````
The function ``getDeclaration`` returns the declaration of the field, as an array.

``checkSetup()``
````````````````````````````````

The ``checkSetup()`` function can be used to check the component configuration. It is called by the ``checkSetup()`` function in the Reef setup class, which is called as soon as a ``Reef`` object is created.
In this method you can check whether any dependencies are present that should be present, and whether there are any incompatibilities.

``validateDeclaration()``
````````````````````````````````

The ``validateDeclaration()`` function is performed whenever a field declaration (possibly belonging to a form definition) is validated. You can override it to perform any custom checks, but be sure you also include the result of this method in your child class implementation. This method is equivalent to ``Component::validateDeclaration``, with the added possibility of using the form context of the field.

``getFlatStructure()``
````````````````````````````````

The ``getFlatStructure()`` function should return an array of columns to be used in the database table for this field. We distinguish three cases:

1. If the field requires multiple columns in the database table, the result of this array should be an associative array with the column name as key and the column specification as value.
2. Otherwise, if the field requires only one column in the table, you may use either an associative array as above, or a numeric array with one entry having key 0 (zero), with as value again a column specification.
3. If the field requires no storage, you may return an empty array ``[]``.

Note that this function is abstract in ``Field``, hence it should always be implemented. A column specification is an array defining what type of table column to create for the field. It can consist of the following entries:

* ``type``: Either ``\Reef\Storage\Storage::TYPE_INTEGER``, ``::TYPE_TEXT``, ``::TYPE_BOOLEAN`` or ``::TYPE_FLOAT``.
* ``min``: For ``::TYPE_INTEGER``, the minimum possible value to be stored
* ``max``: For ``::TYPE_INTEGER``, the maximum possible value to be stored
* ``limit``: For ``::TYPE_TEXT``, the maximum number of characters to be stored (note: characters, not bytes!)


Schema update functions
````````````````````````````````

The ``needsSchemaUpdate()`` function is called before performing a form update (when editing a stored form). By default it returns ``false``, but you may perform custom checks to return ``true`` whenever you need the schema update functions (``beforeSchemaUpdate``, ``afterSchemaUpdate``) to be called. Note that by default, these functions are also always called whenever the flat structure or field name changes with the update. Hence, you only need to implement this method if you have some custom logic going on within your columns.

The functions ``beforeSchemaUpdate``, ``beforeDelete`` and ``afterSchemaUpdate`` are called within the form update process on the moments the names suggest. They are passed a data array with useful variables, for specifics refer to their docblocks.

``updateDataLoss()``
````````````````````````````````

The ``updateDataLoss()`` function can be used to indicate whether a field update will lead to data loss. It is checked before updating a form; if there is (possible) dataloss the user is first asked whether he/she wants to proceed. This method is always called on the new field, and receives the old field instance as first argument. It should return either ``\Reef\Updater::DATALOSS_NO`` if there is a guarantee there is no dataloss, ``::DATALOSS_DEFINITE`` if there is a guarantee there will be dataloss, or ``::DATALOSS__POTENTIAL`` otherwise.

``getOverviewColumns()``
````````````````````````````````

This function should return an array with the same keys as ``getFlatStructure``, but with values containing the title of the fields that can be used in an overview table or CSV file.


View functions
````````````````````````````````
The methods ``view_form`` and ``view_submission`` are called whenever the form HTML or submission HTML are generated, respectively. It receives the ``FieldValue`` to generate the view for as first parameter, and an option array as second. It should return an array of view variables to be used in mustache. You may override this method to modify the result, but then you should always call the parent function and use its result.


``internalRequest()``
````````````````````````````````

Internal requests routed to this field arrive here. Internal requests serve as communication method between the browser and PHP, to aid interactivity. Depending on the request hash (the first argument), you may return anything to the browser, like an image, JSON object, et cetera.
The request paths of fields are formed like ``form:{form_uuid}:field:{field_name}:{custom_request_hash}``, where ``custom_request_hash`` is the hash passed to ``Field::internalRequest()``.
