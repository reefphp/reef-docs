
Creating a component
================================

The best way to create a component is probably by looking how other components are built up, and copying from them. In addition, this guide will help you to better understand what is going on.

Terminology
--------------------------------

Please remember the following terminology, in addition to the `basic terminology <../../users_guide/index.html#terminology>`__:

- A component is a complete set of PHP, HTML, JS & CSS code that defines the behaviour of a specific type of user input, being for instance a text field, checkbox or heading.
- A field is an instance of a component added to a form, complete with filled-in configuration values such as field name (=technical name!), field title (as shown to users), and things like placeholders, default values, required status, visibility conditions et cetera.
- A field value is an object entirely devoted to reading, parsing, validating and saving the value a user submitted to a field.

Additionally:

- The structured storage methods ``toStructured()`` and ``fromStructured()`` define how the value of the field should be registered in PHP. The structured format is a mixed format: it may be e.g. an int, string, (multi-dimensional) array, et cetera.
- The flat storage methods ``toFlat()`` and ``fromFlat()`` define how the value of the field should be stored in the database. It has a more restricted format: ``toFlat()`` should return a one-dimensional array of data, where each entry in the array corresponds to one column in the database table created for the form.
  A component may use any number of entries in the flat representation; either 0, 1 or more, as desired.


Directory structure
--------------------------------

A component has a specific directory structure:
::

    src
    ├── js (or css)
    │   ├── {view_name}.js (e.g. form.js)
    │   └── {layout_name}-{view_name}.js (e.g. bootstrap4-form.js)
    ├── locale
    │   └── {locale_name}.yml (e.g. en_US.yml)
    ├── view
    │   └── {layout_name} (e.g. bootstrap4)
    │       ├── form.mustache
    │       └── submission.mustache
    ├── config.yml
    ├── {view_name}.css (e.g. form.css)
    ├── {view_name}.js (e.g. form.js)
    ├── {component_name}Component.php
    ├── {component_name}Field.php
    └── {component_name}Value.php

Here, ``view_name`` can be either ``form``, ``submission``, ``builder`` or ``all``. When using a specific view, the js/css file is only included when required (i.e., when that view is loaded).
When using all, the js/css file is always included whenever the component is used.

You can put a general JS or CSS file in the root ``src`` dir, or put it in the js/css subdirectory. Additionally, you can place JS and CSS files in the js/css subdirectory with a filename ``{layout_name}-{view_name}.js/css`` to specify code for a specific layout.

In the locale folder, you can add translations in multiple languages. Note that the keys used in this file also need to be registered in the ``config.yml`` file.

In the view folder, all HTML code is situated, using Mustache as templating engine, in PHP as well as in Javascript. For each supported layout, you should add two view files, ``form.mustache`` and ``submission.mustache``.

The file ``config.yml`` defines the general properties of your component.

The three PHP files contain the PHP code of your extension.
The component file contains general functionality, the field file contains functionality for fields (component instances in the form), and the value file contains functionality for parsing, validating and processing submitted values.

Specifics
--------------------------------

Components come with a relatively large number of options you can use, hence we split this guide into four sections:

.. toctree::
   :maxdepth: 1

   config
   component
   field
   field_value
   javascript
