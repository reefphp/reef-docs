
Creating an icon set
================================

Using icon sets, you can customize the icons used within Reef to use your own set of icons. In practice, an icon set is actually just an extension including some hook template files.
For more information on how extensions work, please refer to the page on extensions.
For an example of an icon set extension, you can take a look at the FA4IconSet_.

.. _FA4IconSet: https://gitlab.com/reefphp/reef-extra/fontawesome4
