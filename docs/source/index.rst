.. reef documentation master file, created by
   sphinx-quickstart on Fri Aug 24 20:19:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Reef
================================

.. toctree::
   :maxdepth: 2

   introduction
   users_guide/index
   components/index
   integration_guide/index
   contribution_guide/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

