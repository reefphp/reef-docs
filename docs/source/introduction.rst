
Introduction
================================

This is the documentation for Reef_, the Responsive Embeddable Extensible Form generator for PHP.

Reef is an open source, framework agnostic form builder for PHP, providing functionality for building and showing forms and receiving form submissions from users.
Building forms can be done using either a drag and drop user interface, a chaining PHP interface, or using YAML files or form definition arrays.

Reef requires PHP 7.2 or higher, and depends on the jQuery javascript library.

.. _Reef: https://reefphp.gitlab.io/home/
